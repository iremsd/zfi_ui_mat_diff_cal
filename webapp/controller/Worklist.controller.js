sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
  ],
  function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend(
      "com.arete.zfiuimatdiffcalc.controller.Worklist",
      {
        formatter: formatter,

        onInit: function () {
          var oViewModel;
          oViewModel = new JSONModel({
            OperationsSecimi: ""
          });
          this.setModel(oViewModel, "worklistView");
        },
        onPressOperation: function (oEvent) {
          debugger;
          var that = this,
            oViewModel = this.getModel("worklistView"),
            oDataModel = this.getModel(),
            sSecilenId = oEvent.getSource().getText();
          if (sSecilenId === "Vade Farkı Talebi Olustur") { //talebi
            debugger;
            oViewModel.setProperty("/OperationsSecimi", "T");
          } else if (sSecilenId === "Vade Farkı Kaydı") {
            oViewModel.setProperty("/OperationsSecimi", "K");
            debugger;
          } else if (sSecilenId === "Mail Gönder") { //mail
            oViewModel.setProperty("/OperationsSecimi", "M");
            debugger;
          } else if (sSecilenId === "Cıktı Görüntüle") {
            oViewModel.setProperty("/OperationsSecimi", "G");
            debugger;
          }
          var sSecim = oViewModel.getProperty("/OperationsSecimi"),
            dBudat = null,
            aArr = [],
            oTable = this.getView().byId("SmartTableId").getTable(),
            oItems = oTable.getSelectedIndices();
            
          for (var i = 0; i < oItems.length; i++) {
             var SelectedRow = oTable.getModel().getProperty(oTable.getContextByIndex(oTable.getSelectedIndices()[i]).sPath);
            aArr.push(SelectedRow);
          }
          var  oData = {};
          oData.Operationid = sSecim;
          oData.Budat = dBudat;
          oData.OperationToHesap = aArr;
          debugger;
          oDataModel.create("/OperationSet", oData, {
            success: function (data) {
              debugger;
              oTable.clearSelection();
              sap.m.MessageToast.show("İşlem başarılı!");

            },
            error: function () {
              debugger;
              oTable.clearSelection();
            },
          });
        },
      }
    );
  }
);
