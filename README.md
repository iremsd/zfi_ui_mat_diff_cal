## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Mar 23 2021 09:49:39 GMT+0300 (GMT+03:00)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.5|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.0.48.15:8000/sap/opu/odata/sap/ZFI_VADE_FARKI_SRV
|**Module Name**<br>zfi_ui_mat_diff_calc|
|**Application Title**<br>App Title|
|**Namespace**<br>com.arete|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## zfi_ui_mat_diff_calc

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


